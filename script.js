import http from 'k6/http';
import { check, sleep, fail } from 'k6';
import { Rate, Trend, Counter } from 'k6/metrics';
const myCounter = new Counter('my_counter');


export let options = {
    insecureSkipTLSVerify: true,
    noConnectionReuse: false,
    stages:
        [
            { duration: '10s', target: 100 },
            { duration: '30s', target: 1000 },
            { duration: '50s', target: 500 },
            { duration: '10s', target: 0 },

        ]
};

export let getMetricas = new Trend('getMetricas');
export let getFailRate = new Rate('getFailRate');
export let getSucessRate = new Rate('getSucessRate');
export let getRequisicoes = new Rate('getRequisicoes');

export default () => {

    let res = http.get('https://k8s-api-lno-hml.ecsbr.net/lno/partners-setup/partners-group/9b9c2360-f0d9-4ca3-973f-d96bac31dd36/partners');
    const checkOutput = check(
        res,
        {
            'response code was 200': (res) => res.status == 200,
        },
    );
    if (!checkOutput) {
        fail('unexpected response');
    };
    // console.log('Response time was ' + String(res.timings.duration) + ' ms');
    getMetricas.add(res.timings.duration)
    getFailRate.add(res.status != 200)
    getRequisicoes.add(1)
    getSucessRate.add(res.status === 200)
    myCounter.add(1);
    myCounter.add(2);

    sleep(1);
}

