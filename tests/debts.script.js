// Auto-generated by the postman-to-k6 converter
import { Trend, Rate, Counter } from 'k6/metrics'
import { sleep, check } from "k6";
let ErrorCount = new Counter("errors");
import http from 'k6/http';
// import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
// export function handleSummary(data) {
//   return {
//     "summary.html": htmlReport(data),
//   };
// }
export let PostDuration = new Trend('post_duration')
export let PostFailRate = new Rate('post_fail_rate')
export let PostSucessRate = new Rate('post_sucess_rate')
export let PostReqs = new Counter('post_reqs')

let delta = 200;
export let options = {
  stages: [
    { duration: "10s", target: (delta * 1) },
    { duration: "5s", target: (delta * 1) },

    { duration: "10s", target: (delta * 2) },
    { duration: "5s", target: (delta * 2) },

    { duration: "10s", target: (delta * 3) },
    { duration: "5s", target: (delta * 3) },

    { duration: "10s", target: (delta * 4) },
    { duration: "5s", target: (delta * 4) },

    { duration: "10s", target: (delta * 5) },
    { duration: "5s", target: (delta * 5) },

    { duration: "10s", target: (delta * 6) },
    { duration: "5s", target: (delta * 6) },

    { duration: "10s", target: (delta * 7) },
    { duration: "5s", target: (delta * 7) },

    { duration: "10s", target: (delta * 8) },
    { duration: "5s", target: (delta * 8) },

    { duration: "10s", target: (delta * 9) },
    { duration: "5s", target: (delta * 9) },

    { duration: "10s", target: (delta * 10) },
    { duration: "5s", target: (delta * 10) },

    { duration: "2s", target: (delta * 10) },
    { duration: "2s", target: (delta * 9) },
    { duration: "2s", target: (delta * 8) },
    { duration: "2s", target: (delta * 7) },
    { duration: "2s", target: (delta * 6) },
    { duration: "2s", target: (delta * 5) },
    { duration: "2s", target: (delta * 4) },
    { duration: "2s", target: (delta * 3) },
    { duration: "2s", target: (delta * 2) },
    { duration: "2s", target: (delta * 1) },
    { duration: "2s", target: (delta * 0) }
  ]
};

export default function () {
  const url = "https://k8s-api-lno-hml.ecsbr.net/lno/debts/v1/cpf";
  const params = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  const payload = JSON.stringify({
    "cpf": "09318425066",
    "id": "ffe92f81-29f7-48ce-b23d-47a58562e8b9"
  });
  const response = http.post(url, payload, params);

  PostDuration.add(response.timings.duration)
  PostFailRate.add(response.status == 0 || response.status > 399)
  PostSucessRate.add(response.status == 200)
  PostReqs.add(1)

  let success = check(response, {
    "status is 200": r => r.status === 200
  });
  if (!success) {
    ErrorCount.add(1);
  }
  sleep(5);

}

// const Request = Symbol.for("request");
// postman[Symbol.for("initial")]({
//   options
// });

  // group("Debts", function () {
  //   postman[Request]({
  //     name: "Debts by CPF",
  //     id: "a0843f70-c077-479c-919e-3214a0447877",
  //     method: "POST",
  //     address: "https://k8s-api-lno-hml.ecsbr.net/lno/debts/v1/cpf",
  //     data:
  //       '{\n\t"cpf": "09318425066",\n    "id": "ffe92f81-29f7-48ce-b23d-47a58562e8b9"\n}\n',
  //     headers: {
  //       "Content-Type": "application/json"
  //     }
  //   });
    // postman[Request]({
    //   name: "Debts healthcheck",
    //   id: "9708d290-e58d-4789-a7f8-b22053125f71",
    //   method: "GET",
    //   address: "https://k8s-api-lno-hml.ecsbr.net/lno/debts/health-check",
    //   headers: {
    //     "Content-Type": "application/json"
    //   }
    // });
  // });


