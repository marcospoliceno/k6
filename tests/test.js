// Scenario: Scenario_1 (executor: ramping-arrival-rate)

import { sleep, check } from 'k6'
import http from 'k6/http'

export const options = {
  ext: {
    loadimpact: {
      distribution: { 'amazon:br:sao paulo': { loadZone: 'amazon:br:sao paulo', percent: 100 } },
      apm: [],
    },
  },
  thresholds: {
    http_req_duration: ['p(95)<1000'],
    load_generator_cpu_percent: [{ threshold: 'value<=90', abortOnFail: true }],
    load_generator_memory_used_percent: [{ threshold: 'value<=95', abortOnFail: true }],
  },
  scenarios: {
    Scenario_1: {
      executor: 'ramping-arrival-rate',
      gracefulStop: '30s',
      stages: [
        { target: 50, duration: '10s' },
        { target: 50, duration: '1m30s' },
        { target: 0, duration: '10s' },
      ],
      preAllocatedVUs: 10,
      startRate: 1,
      timeUnit: '60s',
      maxVUs: 20,
      exec: 'scenario_1',
    },
  },
}

export function scenario_1() {
  let response

  // debts - health-check
  response = http.get('https://k8s-api-lno-hml.ecsbr.net/lno/debts/health-check')
  check(response, {
    'body contains alive and kicking': response => response.body.includes('alive and kicking'),
    'status equals 200': response => response.status.toString() === '200',
  })

  // Automatically added sleep
  sleep(5)
}
