import { check } from 'k6'
import { Rate } from 'ke/metrics'
import http from 'k6/http'
import { Trend } from 'k6/metrics'

//check failure Rate or Error Rate
let failureRate = new Rate("failure_rate")

//define trends 
var postDebts = new Trend("Trend_Debts")


export const setHeader = () => {
    return {
        headers: {
            "Content-Type": "application/json"
        }
    }
}